note
	description: "bank application root class"
	date: "$Date$"
	revision: "$Revision$"

class
	APPLICATION

inherit
	ARGUMENTS

create
	make

feature {NONE} -- Initialization

	my_bank:BANK_ACCOUNT

	make
			-- Run application.
		do
			--| Add your code here
			create my_bank.make
			--
			my_bank.set_name ("KAMIL")
			print(my_bank.name)
		end

end
