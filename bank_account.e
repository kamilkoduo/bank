note
	description: "bank account"
	author: "Kamil Akhmetov"
	date: "08.09.2017"
	revision: "$Revision$"

class
	BANK_ACCOUNT

create
	make
feature make
	do
		balance:=DEFAULT_BALANCE
		name:=DEFAULT_NAME
	end
feature --Modify the data
	set_name(a_name: STRING)
	--to set a name to current object
		require
			legal_name: a_name/=Void
		do
			name := a_name
		ensure
			name_has_changed: name = a_name
		end

	deposit(value: INTEGER)
	--to add money to the account(balance)
		require
			legal_value: value>0
		do
			balance:=balance+value
		ensure
			balance_has_increased: balance=old balance + value
		end

	withdraw(value: INTEGER)
	--to withdraw money from the account
		require
			legal_value: value>0
		do
			balance:= balance-value
		ensure
			balance_has_decreased: balance=old balance - value
		end
feature --Attributes

	name:STRING
	--name of the owner

	balance:INTEGER
	--balance of the account

	DEFAULT_NAME: STRING ="default"

	DEFAULT_BALANCE: INTEGER = 100

	MIN_BALANCE: INTEGER =100

	MAX_BALANCE: INTEGER =1000000


invariant
	balance_lower_bound: balance>=MIN_BALANCE
	balance_upper_bound: balance<=MAX_BALANCE
end
